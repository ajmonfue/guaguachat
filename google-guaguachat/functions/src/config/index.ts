import * as functions from 'firebase-functions';

const functionsConfig = functions.config();

interface IConfig {
    TITSA_TRANSIT_API_URL: string;
}

const config: IConfig = {
    TITSA_TRANSIT_API_URL: (functionsConfig.titsatransit && functionsConfig.titsatransit.apiurl) || 'http://localhost:3010'
}

export default config;