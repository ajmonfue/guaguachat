'use strict';
import {dialogflow, Suggestions, Permission, DialogflowConversation, GoogleTypeLatLng, BasicCard, BasicCardOptions, Button} from 'actions-on-google';
import * as functions from 'firebase-functions';
import {TitsaTransitService, IStopTime, IStopInfo} from 'guaguachat-common';
import config from './config';

const titsaTransitService = new TitsaTransitService(config.TITSA_TRANSIT_API_URL);

interface IConvData {
    guaguaNumber: number;
    searchSession: {
        guaguaNumber: number,
        routeId: string,
        serviceId: string
    };
    direction: string;
    predefinedLocation?: string;
    locationCoordinates?: GoogleTypeLatLng;
}

interface IConvUserStorage {
    predefinedLocations: {
        [key: string]: GoogleTypeLatLng
    }
}

const app = dialogflow<IConvData, IConvUserStorage>({debug: true});


app.intent('guagua-time-left', async (conv: DialogflowConversation<IConvData, IConvUserStorage>, {guagua, predefinedLocation}: {guagua: number, predefinedLocation?: string}) => {
    const query = await titsaTransitService.getFinalStops(guagua);
    conv.data.guaguaNumber = guagua;
    conv.data.searchSession = query.session;
    conv.data.predefinedLocation = predefinedLocation;
    conv.ask('Seleccione el sentido de la guagua');
    conv.ask(new Suggestions(query.finalStops));
});

async function resolveGuaguaTimeLeft(conv: DialogflowConversation<IConvData, IConvUserStorage>, final: boolean = true): Promise<any> {
    const coordinates = conv.data.locationCoordinates as GoogleTypeLatLng;
    const searchSession = conv.data.searchSession;
    const guaguaNumber = searchSession.guaguaNumber;
    try {
        const nearStop: IStopInfo = await titsaTransitService.getNearStop(guaguaNumber, {
            latitude: coordinates.latitude as number,
            longitude: coordinates.longitude as number,
            destination: conv.data.direction,
            routeId: searchSession.routeId,
            serviceId: searchSession.serviceId
        });
        const infoArrive: IStopTime = await titsaTransitService.stopTimes(Number(nearStop.stopId), guaguaNumber);
        if (infoArrive) {

            const cardContent: BasicCardOptions = {
                title: `${infoArrive.linea} - ${infoArrive.denominacion}`,
                subtitle: `Destino: ${infoArrive.destinoLinea}`,
                text: `Llegará en ${infoArrive.minutosParaLlegar} minutos`,
                buttons: new Button({title: 'Mas información', url: nearStop.stopUrl})
            }
            conv.ask('Se ha obtenido el siguiente resultado');
            return final? conv.close(new BasicCard(cardContent)) : conv.ask(new BasicCard(cardContent));
        }
        else {
            return conv.close(`No se ha podido obtener el tiempo de espera de la guagua ${guaguaNumber}.`);
        }
    }
    catch(err) {
        return conv.close('Ha ocurrido un error');
    }
}

app.intent('selected-direction', async (conv: DialogflowConversation<IConvData, IConvUserStorage>, {direction}: {direction: string}) => {
    conv.data.direction = direction;
    let context = '';
    if (conv.data.predefinedLocation) {
        if (conv.user.storage.predefinedLocations && conv.data.predefinedLocation in conv.user.storage.predefinedLocations) {
            conv.data.locationCoordinates = conv.user.storage.predefinedLocations[conv.data.predefinedLocation];
            return await resolveGuaguaTimeLeft(conv);
        }
        else {
            context = `No tienes registrado el lugar "${conv.data.predefinedLocation}".`;
        }
    }

    // No se puede tener multiples ask con solicitud de permiso
    // https://github.com/actions-on-google/actions-on-google-nodejs/issues/206
    return conv.ask(new Permission({
        context: context + 'Para buscar la parada más cercana a ti',
        permissions: 'DEVICE_PRECISE_LOCATION',
    }));
});


app.intent('user-location', async (conv: DialogflowConversation<IConvData, IConvUserStorage>, _, permissionGranted) => {
    if (permissionGranted) {
        const coordinates: GoogleTypeLatLng = (conv.device.location && conv.device.location.coordinates) as GoogleTypeLatLng;

        if (coordinates) {
            conv.data.locationCoordinates = coordinates;
            await resolveGuaguaTimeLeft(conv, false);
            conv.ask('¿Quieres registrar esta ubicación?');
            conv.ask(new Suggestions('Sí', 'No'));
        } else {
            // Note: Currently, precise locaton only returns lat/lng coordinates on phones and lat/lng coordinates
            // and a geocoded address on voice-activated speakers.
            // Coarse location only works on voice-activated speakers.
            conv.close('Lo siento. No puedo determinar tu ubicación.');
        }
    }
    else {
        conv.close('Necesito tu ubicación para buscar la parada más cercana a ti.');
    }
});

app.intent('user-location.accept-save-location', (conv: DialogflowConversation<IConvData, IConvUserStorage>) => {
    conv.ask('Por favor, indica el nombre con el que se registrará la ubicación');
    if (conv.data.predefinedLocation) {
        conv.ask(new Suggestions(conv.data.predefinedLocation));
    }
});


app.intent('user-location.save-location', (conv: DialogflowConversation<IConvData, IConvUserStorage>, { locationName }: {locationName: string}) => {
    !conv.user.storage.predefinedLocations && (conv.user.storage.predefinedLocations = {});
    conv.user.storage.predefinedLocations[locationName] = conv.data.locationCoordinates as GoogleTypeLatLng;
    return conv.close(`Se ha guardado la ubicación con el nombre "${locationName}"`);
});


exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);