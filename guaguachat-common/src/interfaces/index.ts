export interface IStopTime {
    codigoParada: string;
    denominacion: string;
    destinoLinea: string;
    hora: string;
    idTrayecto: string;
    linea: string;
    minutosParaLlegar: string;
}

export interface IStopInfo {
    stopId: string;
    stopName: string;
    stopLat: string;
    stopLong: string;
    stopUrl: string;
    _distance?: number;
}

export interface ITrip {
    routeId: string;
    serviceId: string;
    tripId: string;
    tripHeadsign: string;
}
export interface ICalculatedStopTime {
    tripId: string;
    arrivalTime: string;
    departureTime: string;
    stopId: string;
    stopSequence: string
}

export interface IRoute {
    routeId: string;
    agencyId: string;
    routeShortName: string;
    routeLongName: string;
    routeType: string;
    routeColor: string;
    routeUrl: string;
}

export interface ICalendarDate {
    serviceId: string;
    date: string;
    exceptionType: string;
}