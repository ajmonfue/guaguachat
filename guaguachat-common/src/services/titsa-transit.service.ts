import * as got from 'got';
import * as queryString from 'query-string';
import { IStopInfo, IStopTime } from '../interfaces';


export class TitsaTransitService {

    constructor(private readonly apiUrl: string) {}

    public getFinalStops(guaguaNumber: number): Promise<{finalStops: string[], session: any}> {
        return got(`${this.apiUrl}/guagua/${guaguaNumber}/final-stops`, {json: true})
            .then(response => response.body)
            .catch(err => console.error(err))
    }
    
    public getNearStop(guaguaNumber: number, query: {
        latitude: number,
        longitude: number,
        destination: string,
        routeId: string,
        serviceId: string
    }): Promise<IStopInfo> {
        return got(`${this.apiUrl}/guagua/${guaguaNumber}/near-stop`, {
                json: true,
                query: queryString.stringify(query)
            })
            .then(response => response.body)
            .catch(err => console.error(err));
    }

    public stopTimes(stopId: number, guaguaNumber: number): Promise<IStopTime> {
        return got(`${this.apiUrl}/stops/${stopId}/times/${guaguaNumber}`, {json: true})
            .then(response => response.body)
            .catch(err => console.error(err));
    }
    
}