import express from 'express';
import config from './config';
import BusStopSearch from './lib/bus-stop-search';
import ApiTitsaService from './services/api-titsa.service';
import { IStopTime } from 'guaguachat-common';


const api = express();

api.get('/guagua/:guaguaNumber/final-stops', async function (req, res) {
    const guaguaNumber: number = req.params.guaguaNumber;

    const search = new BusStopSearch(guaguaNumber);

    const finalStops =  await search.getFinalStops();

    res.json({
        finalStops,
        session: search.session
    });
})

api.get('/guagua/:guaguaNumber/near-stop', async function (req, res) {
    const guaguaNumber: number = req.params.guaguaNumber;
    const query = req.query;
    
    const search = new BusStopSearch(guaguaNumber);
    const result =  await search.getNearStop({
        location: {
            latitude: query.latitude,
            longitude: query.longitude,
        },
        ...query
    })

    res.json(result);
})


api.get('/stops/:stopId/times/:guaguaNumber', async function (req, res) {
    const stopId: string = req.params.stopId;
    const guaguaNumber: number = req.params.guaguaNumber;
    const result: IStopTime = await ApiTitsaService.getStopTime(stopId, guaguaNumber);

    res.json(result);
})

api.listen(config.PORT, () => {
    console.log(`Servidor corriendo en puerto ${config.PORT}`);
});