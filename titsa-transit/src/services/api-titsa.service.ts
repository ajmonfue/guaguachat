'use strict';

import config from '../config';
import ObjTree from 'objtree';
import axios from 'axios';
import { IStopTime } from 'guaguachat-common';

const xotree = new ObjTree();

export default class ApiTitsaService {
    
    static async getStopTimes(stopId): Promise<IStopTime[] | IStopTime> {
        const res = await axios.get(`${config.TITSA_API_URL}?IdApp=${config.TITSA_IDAPP}&idParada=${stopId}`);
        try {
            const body = xotree.parseXML( res.data );
            return body.llegadas && body.llegadas.llegada;
        }
        catch(e) {
            console.error('Error al obtener los tiempos de la parada: ' + stopId);
            console.error(e);
            return null;
        }
        
    }
    
    static async getStopTime(stopId, linea): Promise<IStopTime> {
        const arrives = await this.getStopTimes(stopId);
        if (arrives) {
            if (arrives instanceof Array) {
                return arrives.find(arrive => arrive.linea == linea);
            }
            else if (arrives instanceof Object &&
                'linea' in arrives &&
                arrives.linea == linea) {
                    return arrives;
            }
        }
        return null;
    }
}