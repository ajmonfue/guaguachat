'use strict';

import fs from 'fs';
import path from 'path';

import { IStopInfo, ITrip, ICalculatedStopTime, IRoute, ICalendarDate } from 'guaguachat-common';

function getFileContent(relPath){
    const filePath = path.join(__dirname, relPath);
    return fs.readFileSync(filePath, { encoding: 'utf8' });
}

function csv2js<S, T>(relPath, mapper?: (obj: S) => T, { separator = ',' } = {}): T[]{
    const content = getFileContent(relPath);
    const arrayLines: any[] = content.split(/\r\n/g);
    const header = arrayLines.shift().split(separator).map((field: string) => field.replace(/\ufeff/g, ''));
    
    return arrayLines.reduce((arrayLines, line ) => {
        if (line != '') {
            const obj = line.split(separator).reduce((obj, fieldValue, i) => {
                obj[ header[i] ] = fieldValue;
                return obj;
            }, {})
            arrayLines.push(mapper? mapper(obj): obj);
        }
        return arrayLines;
    }, []);
}

export interface ITransitData {
    calendarDates: ICalendarDate[];
    routes: IRoute[];
    stopTimes: ICalculatedStopTime[];
    stops: IStopInfo[];
    trips: ITrip[];
}

interface ICSVTrip {
    route_id: string;
    service_id: string;
    trip_id: string;
    trip_headsign: string;
}

interface ICSVStopInfo {
    stop_id: string;
    stop_name: string;
    stop_lat: string;
    stop_lon: string;
    stop_url: string;
}
interface ICSVCalculatedStopTime {
    trip_id: string;
    arrival_time: string;
    departure_time: string;
    stop_id: string;
    stop_sequence: string
}
interface ICSVRoute {
    route_id: string;
    agency_id: string;
    route_short_name: string;
    route_long_name: string;
    route_type: string;
    route_color: string;
    route_url: string;
}

interface ICSVCalendarDate {
    service_id: string;
    date: string;
    exception_type: string;
}


export const TransitData: ITransitData = {
    calendarDates: csv2js<ICSVCalendarDate, ICalendarDate>('Google_transit/calendar_dates.txt', (obj => ({
        serviceId: obj.service_id,
        date: obj.date,
        exceptionType: obj.exception_type
    }))),
    routes: csv2js<ICSVRoute, IRoute>('Google_transit/routes.txt', (obj) => ({
        routeId: obj.route_id,
        agencyId: obj.agency_id,
        routeShortName: obj.route_short_name,
        routeLongName: obj.route_long_name,
        routeType: obj.route_type,
        routeColor: obj.route_color,
        routeUrl: obj.route_url
    })),
    stopTimes: csv2js <ICSVCalculatedStopTime , ICalculatedStopTime>('Google_transit/stop_times.txt', (obj) => ({
        tripId: obj.trip_id,
        arrivalTime: obj.arrival_time,
        departureTime: obj.departure_time,
        stopId: obj.stop_id,
        stopSequence: obj.stop_sequence
    })),
    stops: csv2js<ICSVStopInfo, IStopInfo>('Google_transit/stops.txt', (obj ) => ({
        stopId: obj.stop_id,
        stopName: obj.stop_name,
        stopLat: obj.stop_lat,
        stopLong: obj.stop_lon,
        stopUrl: obj.stop_url
    })),
    trips: csv2js<ICSVTrip, ITrip>('Google_transit/trips.txt', (obj ) => ({
        routeId: obj.route_id,
        serviceId: obj.service_id,
        tripId: obj.trip_id,
        tripHeadsign: obj.trip_headsign
    })),
}