'use strict';
import fs from 'fs';
import path from 'path';

if (fs.existsSync(path.join(__dirname, 'secrets.json'))) {
    console.log('Obteniendo variables de configuración de secrets.json');
    Object.assign(process.env, require('./secrets.json'));
}

interface IConfig {
    PORT: number;
    TITSA_IDAPP: string;
    TITSA_API_URL: string;
}

const config: IConfig = {
    PORT: Number(process.env.PORT) || 3010,
    TITSA_IDAPP: process.env.TITSA_IDAPP,
    TITSA_API_URL: process.env. TITSA_API_URL || 'http://apps.titsa.com/apps/apps_sae_llegadas_parada.asp'
}

export default config;