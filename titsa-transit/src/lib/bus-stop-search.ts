'use strict';
import { TransitData } from '../data/transit-data';
import { IStopInfo, ITrip } from 'guaguachat-common';

interface IQueryParams {
    location: {latitude: any, longitude: any};
    destination: string;
    routeId: string;
    serviceId: string
}
function distanceBetweenCoordinates({lat: lat1, lng: lon1}, {lat: lat2, lng: lon2}): number {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
    c(lat1 * p) * c(lat2 * p) * 
    (1 - c((lon2 - lon1) * p))/2;
    
    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

export default class BusStopSearch {
    private date: string;
    
    constructor(public guaguaNumber: number){
        this.date = (new Date()).toISOString().substring(0, 10).replace(/-/g,'');
        this.guaguaNumber = Number(guaguaNumber);
    }
    
    async getServiceId(routeId, allTrips: ITrip[]) {
        return TransitData.calendarDates
                            .find(d => d.date == this.date && allTrips.find(trip => trip.serviceId == d.serviceId && trip.routeId == routeId)).serviceId;
    }

    async getRouteId(){
        return TransitData.routes.find(d => Number(d.routeShortName) == this.guaguaNumber).routeId;
    }
    
    async filterTrips(routeId){
        return TransitData.trips.filter(trip => trip.routeId == routeId);
    }
    
    
    async getTripId(queryParams: IQueryParams){
        return TransitData.trips.find(d => d.routeId == queryParams.routeId &&
            (d.tripHeadsign as string).toLocaleLowerCase() == (queryParams.destination as string).toLocaleLowerCase() && d.serviceId == queryParams.serviceId).tripId;
    }
    
    
    async getFinalStops(): Promise<string[]>{
        
        const routeId = await this.getRouteId();
        const allTrips: ITrip[] = await this.filterTrips(routeId);
        const serviceId = await this.getServiceId(routeId, allTrips);
        
        // Session porpuse
        Object.assign(this, {routeId, serviceId});
        
        const setFinalStops = allTrips.reduce((set, d) => {
            if (d.routeId == routeId && d.serviceId == serviceId) {
                set.add(d.tripHeadsign);
            }
            return set;
        }, new Set<string>());
        
        return Array.from(setFinalStops);
    }
    
    get session() {
        return {...this};
    }
    
    async getNearStop(queryParams: IQueryParams): Promise<IStopInfo>{        
        const tripId = await this.getTripId(queryParams);
        
        const stops = await this.getStops(tripId);
        
        const location = queryParams.location;
        
        return stops
            .map(stop => {
                stop = {...stop};
                stop._distance = distanceBetweenCoordinates(
                    {
                        lat: location.latitude,
                        lng: location.longitude
                    },
                    {
                        lat: stop.stopLat,
                        lng: stop.stopLong
                    }
                )
                return stop;
            })
            .sort((a, b) => a._distance - b._distance)[0];
    }

    getStops(tripId: string): IStopInfo[] {
        return TransitData.stopTimes
            .filter(stopTime => stopTime.tripId == tripId)
            .map(stopTime => TransitData.stops.find(stop => stop.stopId == stopTime.stopId));
    } 
}
