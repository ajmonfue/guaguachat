'use strict';

import Scene from 'telegraf/scenes/base';
import Markup from 'telegraf/markup';
import Router from 'telegraf/router';

import { IStopInfo, IStopTime } from 'guaguachat-common';
import titsaTransitService from '../lib/titsa-transit.service';


const timer = new Scene('timer');

timer.enter( async (ctx) => {
  const guaguaNumber = ctx.state.guaguaNumber;

  const query = await titsaTransitService.getFinalStops(guaguaNumber);
  const finalStops =  await query.finalStops;

  await ctx.reply(
    'Seleccione el destino de la guagua',
    Markup
      .inlineKeyboard(finalStops.map(s => Markup.callbackButton(s, `selectSense:${s}`)))
      .oneTime()
      .extra()
  )
  ctx.session.searchSession = query.session;
})


timer.on('location', async (ctx) => {
  ctx.reply('Localización recibida', {
    reply_markup: {
      remove_keyboard: true
    }
  });

  const { searchSession } = ctx.session;
  const { guaguaNumber } = searchSession;
  if ( guaguaNumber && searchSession) {
    try {
      const nearStop: IStopInfo = await titsaTransitService.getNearStop(guaguaNumber, {
        latitude: ctx.message.location.latitude,
        longitude: ctx.message.location.longitude,
        destination: searchSession.destination,
        routeId: searchSession.routeId,
        serviceId: searchSession.serviceId
      });
      await ctx.replyWithLocation(nearStop.stopLat, nearStop.stopLong);
      const infoArrive: IStopTime = await titsaTransitService.stopTimes(Number(nearStop.stopId), guaguaNumber);

      if (infoArrive) {
        const minutes = Number(infoArrive.minutosParaLlegar);
        await ctx.reply(`La guagua tardará en llegar ${minutes} ${ minutes== 1? 'minuto': 'minutos'}`);
      }
      else {
        await ctx.reply(`No se ha podido obtener el tiempo de espera de la guagua ${guaguaNumber}.`);
      }

      
      ctx.session.searchSession = null;
    }
    catch(err) {
      ctx.reply(`Ha ocurrido un error: ${err.message}`);
      throw err;
    }

  }
  else {
    ctx.reply('Acción no permitida');
  }
})



const selectable = new Router((ctx) => {

  const callbackQuery = ctx.callbackQuery;

  if (!callbackQuery.data) return;

  ctx.editMessageReplyMarkup({ inline_keyboard:[]  });
  
  const parts = callbackQuery.data.split(':');

  return {
    route: parts[0],
    state: {
      senseName: parts[1]
    }
  }
  
})

selectable.on('selectSense', async (ctx) => {
  ctx.session.searchSession.destination = ctx.state.senseName;
  await ctx.reply(`Sentido seleccionado ${ctx.state.senseName}`);
  await ctx.reply('Envíe su ubicación', {
    "parse_mode": "Markdown",
    "reply_markup": {
        "one_time_keyboard": true,
        "keyboard": [
          [{
            "text": "Mi ubicación",
            "request_location": true
          }],
          ["Cancel"]
        ]
    }
  });
})

timer.on('callback_query', selectable);

export default timer;