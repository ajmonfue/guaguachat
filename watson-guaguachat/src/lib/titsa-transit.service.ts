import { TitsaTransitService } from 'guaguachat-common';
import config from '../config';

const titsaTransitService = new TitsaTransitService(config.TITSA_TRANSIT_API_URL);

export default titsaTransitService;