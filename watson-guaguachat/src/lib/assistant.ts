'use strict';

import AssistantV1, { MessageResponse } from 'ibm-watson/assistant/v1';
import config from '../config';


export default class Assistant {
  private _assistant: AssistantV1;
  constructor(configAssistant){
    this._assistant = new AssistantV1(configAssistant);
  }

  message(params): Promise<MessageResponse>{
    if (!params.workspace_id)
      params.workspace_id = config.WATSON_ASSISTANT.workspace_id;
    
    return new Promise((resolve, reject) => {
      this._assistant.message(params, (err, response) => {
        if (err) {
          return reject(err);
        }
        return resolve(response);
      })
    });
  }
}