'use strict';

import express from 'express';
import Telegraf from 'telegraf';
import firebaseSession from 'telegraf-session-firebase'
import admin from 'firebase-admin';

import Stage from 'telegraf/stage';

import Assistant from './lib/assistant';
import config from './config';
import serviceAccount from './config/serviceAccount.json';
import timer from './scenes/timer';
import { MessageResponse } from 'ibm-watson/assistant/v1';


const app = express();
const assistant = new Assistant(config.WATSON_ASSISTANT);
const bot = new Telegraf(config.TELEGRAM_BOT_TOKEN);
const guaguaNumberRegex = /\/time\s+([0-9]{1,3})/;


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as any),
  databaseURL: "https://guaguachat.firebaseio.com"
});

const database = admin.database();
const stage = new Stage([timer]);


bot.use(firebaseSession(database.ref('sessions')));
bot.use(stage.middleware());

bot.command('time', async (ctx: any) => {
  const text = ctx.message.text;
  const parts = text.match(guaguaNumberRegex);

  if (parts) {
    ctx.state.guaguaNumber = parts[1];
    await ctx.scene.enter('timer');
  }
  else {
    ctx.reply('Introduzca un numero de guagua válido.');
  }
});


bot.on('text', async (ctx: any) => {
  const text = ctx.message.text;

  try {
    const response: MessageResponse = await assistant.message({
      input: { text },
      context: ctx.session.watsonContext
    });

    const context = response.context;

    if (context.guaguaNumber) {
      ctx.state.guaguaNumber = context.guaguaNumber;
      await ctx.scene.enter('timer');
      context.guaguaNumber = null;
    }
    else {
      ctx.reply(response.output.text[0]);
    }

    ctx.session.watsonContext =  context;
    
  }
  catch (err) {
    ctx.reply(err.message || 'algo salió mal...');
  }
  
});

bot.catch(err => {
  console.log('Ha ocurrido un error:', err);
});

bot.startPolling();

app.listen(config.PORT, () => {
  console.log(`Servidor corriendo en puerto ${config.PORT}`);
});