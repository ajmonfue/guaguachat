'use strict';
import secrets from './secrets.json';

interface IConfig {
  PORT: number;
  WATSON_ASSISTANT: any;
  TELEGRAM_BOT_TOKEN: string;
  TITSA_TRANSIT_API_URL: string;
}

const config: IConfig = {
  PORT: Number(process.env.PORT) || 3000,
  WATSON_ASSISTANT: secrets.WATSON_ASSISTANT,
  TELEGRAM_BOT_TOKEN: secrets.TELEGRAM_BOT_TOKEN,
  TITSA_TRANSIT_API_URL: 'http://localhost:3010'
}

export default config;